package com.example.kafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.service.KafkaProducer;
import org.springframework.web.bind.annotation.PostMapping;


@RestController
@RequestMapping("api")
public class KafkaController {
    
    @Autowired
    KafkaProducer kafkaProducer;

    @PostMapping("/message")
    public void sendMessage(@Validated @RequestParam String message){      
        
         kafkaProducer.sendMessage(message);
        
    }
}
