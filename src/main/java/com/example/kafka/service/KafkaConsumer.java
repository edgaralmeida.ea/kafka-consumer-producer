package com.example.kafka.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    @Value("${kafka.topic}")
    String topic;

    @KafkaListener(topics = "${kafka.topic}", groupId = "test")
    public void consumeMessage(String message) {

        System.out.println("Received Message: " + message + " from topic: " + topic);

    }

}
