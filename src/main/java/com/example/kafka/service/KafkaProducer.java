package com.example.kafka.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducer {


    @Autowired
    KafkaTemplate<String,String> kafkaTemplate;

    @Value("${kafka.topic}")
    String topic;

    public void sendMessage(String message){

        kafkaTemplate.send(topic, message);

        System.out.println("Message: " + message + " sent to topic: " + topic);

    }
    
}
