# Kafka Consumer Producer

Basic example of a spring boot applica consuming and producing Apache Kafka topics

# Install Apache Kafka and Zookeeper

Start zookepper -> bin/windows/zookeeper-server-start.bat config/zookeeper.properties

Start kafka server from your kafka folder -> bin/windows/kafka-server-start.bat config/server.properties


# Run the application 


### Swagger Ui
http://localhost:8010/swagger-ui.html

### Example
POST Endpoint: api/message
Param: String Message

Call the endpoint: 'http://localhost:8080/api/message?message=Hi%20i%20am%20a%20kafka%20topic'

Result log ->  

Producer log -> Message: I am a kafka message sent to topic: test_topic

Consumer log -> Received Message: I am a kafka message from topic: test_topic

